import os
import socket

from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello():
    html = (
        "<h3>Hello {name}!</h3>"
        "<b>Hostname:</b> {hostname}<br/>"
        "<b>Username:</b> {username}<br/>"
    )
    return html.format(
        name=os.getenv("NAME"),
        hostname=socket.gethostname(),
        username=os.getenv("USERNAME"),
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
